#!/usr/bin/python3
# -*- coding: utf-8 -*-

from element import Element
from tradisparser import TradisParser
from agendaparser import AgendaParser

from datetime import datetime

class MainModel:
    def __init__(self, view, args):
        self._view = view
        self.clear()
        
        self._args = args
        
    def nextElement(self):
        if len(self._elements):
            self._id += 1
            self._id %= len(self._elements)

            self.notifyElement()
        
    def prevElement(self):
        if len(self._elements):
            self._id -= 1
            self._id %= len(self._elements)

            self.notifyElement()
        
    def elementAt(self, index):
        if index < len(self._elements):
            self._id = index
        
        self.notifyElement()
        
    def notifyOverview(self):
        self._view.updateOverview(self._id, self._elements, self._location, self._date)

    def notifyElement(self):
        if len(self._elements):
            self._view.updateElement(self._id, self._elements[self._id])
            
    def notify(self):
        self._view.update(self._id, self._elements)

    def proVote(self, value):
        self._elements[self._id].vote().setPro(value)
        
        self.notifyElement()
        
    def blankVote(self, value):
        self._elements[self._id].vote().setBlank(value)
        
        self.notifyElement()
        
    def conVote(self, value):
        self._elements[self._id].vote().setCon(value)
        
        self.notifyElement()

    def saveNotes(self, text):
        self._elements[self._id].setNotes(text)
    
    def clear(self):
        self._id = 0
        self._elements = []
        self._location = ""
        self._date = None
            
    def importFile(self, filename):
        elements, location, date = AgendaParser.parse(filename)
        self.load(elements, location, date)
        
    def openFile(self, filename):
        elements, location, date = TradisParser.parse(filename)
        self.load(elements, location, date)
        
    def importUrl(self, url):
        elements, location, date = AgendaParser.parseUrl(url)
        self.load(elements, location, date)
    
    def load(self, elements, location, date):
        self.clear()
        self._elements = elements
        self._location = location
        self._date = date
        
        self.notify()
        self.notifyElement()
        self.notifyOverview()
        
    def saveFile(self, filename):
        TradisParser.write(filename, self._elements, self._location, self._date)
        
    def saveDraft(self):
        pass
        
    def processArgs(self):
        if self._args.import_file:
            self.importFile(self._args.import_file)
        elif self._args.import_url:
            self.importUrl(self._args.import_url)
        elif self._args.open_file:
            self.openFile(self._args.open_file)
        
        if self._args.output_file:
            pass
        
        self.notify()
        self.notifyOverview()
