.p La prochaine séance du Conseil communal aura lieu le lundi 3 décembre à 20h dans la salle du conseil, Grand'Place Baudouin Ier, 3 (2e étage).
.h Ordre du jour
.ul Installation du Conseil communal
..ul Prestation de serment des Conseillers
..ul Fixation provisoire de l'ordre de préséance des Conseillers.
.ul Adoption d'un pacte de majorité, désignation des membres du Collège communal et prestation de serment du Bourgmestre et des Echevins.
.ul Désignations des membres du Conseil de l'Action sociale.
.ul Marchés communaux de travaux, de fournitures et de services – Délégation au Collège communal.
.ul Désignation du personnel contractuel – Délégation au Collège communal.
