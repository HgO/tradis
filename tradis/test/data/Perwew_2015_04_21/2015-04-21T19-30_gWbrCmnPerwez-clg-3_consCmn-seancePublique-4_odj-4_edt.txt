
﻿
          - "Perwez, le 13 avril 2015
          - "Conformément aux dispositions de l’Arrêté du Gouvernement wallon du 22 avril 2004 portant codification de la législation relative aux pouvoirs locaux, nous avons l'honneur de vous convoquer, à la séance du Conseil communal qui aura lieu le mardi 21 mars 2015 à 19 h 30 en la salle du Conseil de l’Hôtel de ville, rue Emile de Brabant 2 à 1360 PERWEZ.
Préalablement à l’examen de l’ordre jour, le Conseil recevra le club de tennis de table de PERWEZ championne avec son équipe première en Nationale 2 et qui pour la première fois de leur histoire évoluera en 1ère nationale la saison prochaine.
Le Conseil recevra également Mademoiselle Marie GAZIAUX candidate perwézienne à l’émission télévisée « The Voice Belgium ».

        - "Séance publique
        - h:
          - "DEVELOPPEMENT TERRITORIAL
          - li class="obj":
          - "AMENAGEMENT DU TERRITOIRE
          - li class="obj":
            - "1. Acquisition d’un bien sis rue aux Quatre vents à PERWEZ – Approbation de principe – Décision – 2.073.511.1/js
            - "2. Modification de l’emprise du chemin n°7 et modifications du sentier n°29 – Ruelle Dinant à Thorembais-Saint-Trond et cadastré 5ème division, section C n°s 490L, 490M, 490N et 497N – Approbation définitive – 2.073.511.1/js
            - "3. Rue de Glatigny à Thorembais-les-Béguines – Cession gratuite d’une partie de terrain et aménagement de voirie – Propriété de           - "Monsieur Frédéric HOSLET, cadastrée 4ème division, section C n°s 83R3, 83S3 et 75D – Décision – 2.073.511.1/js
          - "LOGEMENT
          - li class="obj":
            - "4. Logements « Tremplin » sis avenue des Tourterelles – Règlement d’ordre intérieur – Décision – 2.073.513.2/bc
          - "MARCHES PUBLICS
          - li class="obj":
            - "Marché de services
            - li class="obj":
            - "5. Marché de services – Remplacement de tubes fluorescents par des tubes LED à l’école communale de Malèves – Choix du mode de passation – Fixation des conditions – Décision – 2.073.515.12/fm
            - "Marchés de travaux
            - li class="obj":
            - "6. Marché de travaux – Réfection de la maison des associations et de son annexe, rue du culot 4 à 1360 PERWEZ (Thorembais-Saint-Trond), en classes – Choix du mode de passation – Fixation des conditions – Décision – 1.851.162/jpf
            - "7. Marché de travaux – Aménagement des locaux situés à l’arrière du kibboutz en classes – Choix du mode de passation du marché – Fixation des conditions – Décision – 1.851.162/jpf
Marchés de fournitures
            - "8. Marché de fournitures – Acquisition de petit matériel de voirie pour les services techniques communaux – Choix du mode de passation du marché – Fixation des conditions – Décision – 2.073.535/jpf
            - "9. Marché de fournitures – Acquisition de mobilier divers pour l’enseignement maternel et primaire – Choix du mode de passation du marché – Fixation des conditions – Décision – l.851.163/ju
          - "SECRÉTARIAT
          - li class="obj":
            - "10. Provinciale Brabançonne d’Energie (PBE) – Assemblée générale extraordinaire du vendredi 29 mai 2015 – Points portés à l’ordre du jour :
            - li class="obj":
              - "- Modification des statuts
            - li class="obj":
              - "- Prise d’acte de la désignation du membre avec voix consultative au Conseil d’Administration (non obligatoire)
            - li class="obj":
              - "- Démission des administrateurs et membres du Collège du commissaire et nomination d’experts
– Approbation – Décision – 1.824.112/lb (retrait du Conseil du 17 mars 2015)
            - "11. Provinciale Brabançonne d’Energie (PBE) – Assemblée générale ordinaire du vendredi 29 mai 2015 – Points portés à l’ordre du jour :
            - li class="obj":
              - "- Comptes annuels
            - li class="obj":
              - "- Décision de distribuer et de fixer un premier dividende
            - li class="obj":
              - "- Décision de distribuer et de fixer un deuxième dividende
            - li class="obj":
              - "- Décharge aux administrateurs, au commissaire-réviseur et aux membres du Collège des commissaires
            - li class="obj":
              - "- Nominations statutaires
            - li class="obj":
              - "- Fixation du jeton de présence et des frais de déplacement
            - li class="obj":
              - "- Notifications : affiliations Infra-X-net et InfraGIS
            - li class="obj":
              - "- Tour de table
– Approbation – Décision – 1.824.112/lb
            - "12. Intercommunale IMIO – Assemblée générale du jeudi 04 juin 2015 – Points portés à l’ordre du jour :
            - li class="obj":
              - "- Présentation du rapport de gestion du Conseil d’Administration
            - li class="obj":
              - "- Présentation du rapport du Collège des contrôleurs aux comptes
            - li class="obj":
              - "- Présentation et approbation des comptes 2015
            - li class="obj":
              - "- Décharge aux administrateurs
            - li class="obj":
              - "- Décharge aux membres du Collège des contrôleurs aux comptes
            - li class="obj":
              - "- Evaluation du plan stratégique
            - li class="obj":
              - "- Désignation d’administrateurs
            - li class="obj":
              - "- Désignation d’un collège de 2 réviseurs – Attribution
– Approbation – Décision – 2.073.532.1/lb
            - "13. CREADIV s.a. – Assemblée générale du vendredi 12 juin 2015 – Points portés à l’ordre du jour :
            - li class="obj":
              - "- Comptes annuels 2014
            - li class="obj":
              - "- Décharge aux administrateurs
            - li class="obj":
              - "- Décharge au commissaire-réviseur
            - li class="obj":
              - "- Nomination statutaire – Nomination d’un commissaire -réviseur
            - li class="obj":
              - "- Divers – Tour de table
– Approbation – Décision – 1.824.112/lb
            - "14. Asbl GAL Culturalité en Hesbaye – Bilan moral et comptes financiers 2014 – Perspectives 2015 – Communication – 1.82/lb
            - "15. Fabrique d’église Saint-Lambert à Orbais – Composition du Conseil de fabrique et du bureau des marguilliers – Communication – 1.857.075.1.074.13/lb
            - "16. Centre Public d’Action Sociale – Commission Locale pour l’Energie (CLE) – Composition – Rapport d’activités de l’année 2014 – Information – 1.842.545/lb
            - "17. Motion sur les négociations en vue d'un projet de Partenariat transatlantique de commerce et d'investissement entre l'Union européenne et les Etats-Unis et ses conséquences sur les entités locales – Décision – 1.858/cr
          - "AGENCE DE DEVELOPPEMENT LOCAL
          - li class="obj":
            - "18. Asbl A.D.L. de PERWEZ – Budget 2015 – Résultat des comptes – Rapport d’activités 2014 – Présentation – Avis – 1.836.1/rr
            - "19. Asbl A.D.L. de PERWEZ – Renouvellement de l’agrément – Maintien – Décision – 1.836.1/rr
          - "COMMISSION LOCALE DE DEVELOPPEMENT RURAL
          - li class="obj":
            - "20. Commission locale de développement rural (C.L.D.R.) – Rapport d’activités 2014 – Approbation – 1.836.1/jlb
          - "PERSONNEL COMMUNAL
          - li class="obj":
            - "21. Bibliothèque de PERWEZ – Demande de reconnaissance pour un opérateur associatif direct – Collège du 25 mars 2015 – Ratification – 1.852.11/st

        - "Séance à huis clos
        - h:
          - "PERSONNEL COMMUNAL
          - li class="obj":
            - "1. Personnel communal – Infractions en matière d’environnement – Agent constatateur – Désignation – 2.08/st
          - "ENSEIGNEMENT COMMUNAL
          - li class="obj":
            - "2. Désignation d’une institutrice maternelle à titre temporaire dans un emploi vacant à raison de 13 périodes par semaine à l’école communale de MALEVES, Implantation d’ORBAIS, et ce, du 9 mars 2015 au 30 juin 2015 – Collège du 11 mars 2015 – Ratification – 1.851.11.08/IR/dr/gs
            - "3. Désignation d’une institutrice maternelle à titre temporaire dans un emploi vacant à raison de 13 périodes par semaine à l’école communale de PERWEZ, et ce, du 9 mars 2015 au 30 juin 2015 – Collège du 11 mars 2015 – Ratification – 1.851.11.08/IR/dr/gs
            - "4. Désignation d’une institutrice maternelle à titre temporaire dans un emploi non vacant à raison de 26 périodes par semaine à l’école communale de THOREMBAIS-LES-BEGUINES, en remplacement de la titulaire écartée par mesure de protection de la maternité, et ce, à partir du 9 mars 2015 et jusqu’au plus tard le 30 juin 2015 – Collège du 11 mars 2015 – Ratification – 1.851.11.08/VD/dr/gs
            - "5. Désignation d’une institutrice primaire à titre temporaire dans un emploi non vacant à raison de 24 périodes par semaine à l’école communale de PERWEZ, en remplacement de la titulaire en congé de maladie, et ce, à partir du 4 mars 2015 et jusqu’au plus tard le 30 juin 2015 – Collège du 11 mars 2015 – Ratification – 1.851.11.08/GC/dr/gs
            - "6. Désignation d’une institutrice primaire à titre temporaire dans un emploi non vacant à raison de 13 périodes par semaine à l’école communale de MALEVES, en remplacement de la titulaire en congé de maladie, et ce, à partir du 23 février 2015 et jusqu’au plus tard le 30 juin 2015 – Collège du 11 mars 2015 – Ratification – 1.851.11.08/SW/dr/gs
            - "7. Désignation d’une institutrice primaire à titre temporaire dans un emploi non vacant à raison de 5 périodes par semaine à l’école communale de MALEVES, en remplacement de la titulaire en congé de maladie, et ce, à partir du 23 février 2015 et jusqu’au plus tard le 30 juin 2015 – Collège du 11 mars 2015 – Ratification – 1.851.11.08/ML/dr/gs
            - "8. Désignation d’une institutrice primaire à titre temporaire dans un emploi non vacant à raison de 18 périodes par semaine à l’école communale de THOREMBAIS-SAINT-TROND, en remplacement de la titulaire, et ce, à partir du 23 février 2015 et jusqu’au plus tard le 30 juin 2015 – Collège du 11 mars 2015 – Ratification – 1.851.11.08/AN/dr/gs
            - "9. Désignation d’une institutrice primaire à titre temporaire dans un emploi non vacant à raison de 24 périodes par semaine à l’école communale de PERWEZ, en remplacement de la titulaire en congé de maladie, et ce, à partir du 17 mars 2015 et jusqu’au plus tard le 30 juin 2015 – Collège du 25 mars 2015 – Ratification – 1.851.11.08/AN/dr/gs
            - "10. Désignation d’un Maître spécial de psychomotricité à titre temporaire dans un emploi vacant à raison de 2 périodes par semaine à l’école communale de PERWEZ, et ce, du 18 mars 2015 jusqu’au 30 juin 2015 – Collège du 25 mars 2015 – Ratification – 1.851.11.08/SK/dr/gs

          - "Arrêté par le Collège communal du mercredi 08 avril 2015

        - "Le Directeur général, 
        - "Le Bourgmestre f.f.,
Michel RUELLE Carl CAMBRON
Extraits du règlement d'ordre intérieur du conseil communal
Article 20
Sans préjudice de l'article 22, pour chaque point de l'ordre du jour des réunions du conseil communal, toutes les pièces se rapportant à ce point – en ce compris le projet de délibération visé à l'article 10 du présent règlement - sont mises à la disposition, sans déplacement, des membres du conseil, et ce, dès l'envoi de l'ordre du jour.
Durant les heures d'ouverture des bureaux du secrétariat, les membres du conseil communal peuvent consulter ces pièces auprès du secrétaire communal ou de son délégué.
Article 21
Les membres du conseil communal désireux que des informations techniques au sujet des documents figurants aux dossiers dont il est question à l’article 20 leur soient fournies conviennent avec le secrétaire communal des jour et heure auxquels ils lui feront visite.
Durant les heures d'ouverture des bureaux, les fonctionnaires communaux désignés par le secrétaire communal fournissent aux membres du conseil communal qui le demandent des informations techniques au sujet des documents figurant aux dossiers dont il est question à l'article 20.
