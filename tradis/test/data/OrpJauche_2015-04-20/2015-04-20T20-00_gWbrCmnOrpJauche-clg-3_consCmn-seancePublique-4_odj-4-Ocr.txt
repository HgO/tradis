Province du Brabant wallon
Arrondissement de Nivelles
COMMUNE D'ORP-JAUCHE
CONVOCATION DU CONSEIL COMMUNAL
Orp-Jauche, le 09 avril 2015.
Madame, Monsieur, Cher(e) coll�gue,
Conform�ment � l'article L1122-13 �1er du Code de la D�mocratie Locale et de la D�centralisation et au R�glement d'Ordre Int�rieur, nous avons l'honneur de vous inviter � participer � la s�ance du conseil communal qui aura lieu le lundi 20 avril 2015 � 20 heures, � la salle du Conseil communal, Place communale 6 � 1350 Orp-Jauche.
L'ordre du jour de cette assembl�e est reproduit en annexe.
Dans l'attente du plaisir de vous rencontrer, nous vous prions d'agr�er, Madame, Monsieur, l'assurance de notre consid�ration tr�s distingu�e.
Pour le Coll�ge communal, Par ordonnance,
La Directrice g�n�rale, Le Bourgmestre,
Sabrina SANTUCCI
Hugues GHENNE
ORDRE DU JOUR : (premi�re convocation)
1. SECRETARIAT
1.1. Application du droit � interpellation du public;
1.2. Lecture de la r�ponse du Coll�ge � l'interpellation du public du 23 mars 2015;
1.3. Approbation du proc�s-verbal de la s�ance du 23 mars 2015;
2. COMPTABILITE
2.1. Tutelle sp�ciale d'approbation : approbation du budget de l'exercice 2015 du Centre public d'Action sociale ;
2.2. Garantie bancaire en faveur de l'Association Chapitre XII Eug�ne Malev� pour la ligne de cr�dit aupr�s de Belfius Banque ;
2.3. Octroi d'un subside de fonctionnement en faveur de l'ASBL Office du Tourisme d'Orp-Jauche pour l'exercice 2015 ;
3. PLAN DE COHESION SOCIALE
3.1.       Approbation des rapports d'activit� et financier 2014 ;
4. FABRIQUES D'EGLISE
4.1. Approbation des comptes 2014 de la Fabrique d'�glise Saints Martin et Ad�le d'Orp-le-Grand ;
5. MARCHES DE TRAVAUX
5.1. March� de travaux ayant pour objet la r�paration des malfa�ons dans le cadre de la r�fection de l'�gouttage et de la voirie rue Pierre Renard (partie sup�rieure) -
D�cision de principe, approbation du CSCH, des conditions de march� et du mode de passation ;
HUIS CLOS.
6. PERSONNEL
6.1. Agent biblioth�caire statutaire - R�gularisation r�troactive par une mise � la retraite d'office au 01.05.2010, une modification du statut de d�finitif en contractuel au 01.10.2011 et une d�cision de fin de fonction d'agent contractuel pensionn� au 30.04.2015;
7. ENSEIGNEMENT
7.1. Nominations � titre d�finitif au 1er avril 2015 ;
7.2. Ratification de la d�cision prise par le Coll�ge communal en sa s�ance du 16 mars 2015 et ayant trait au remplacement d'une institutrice maternelle � l'�cole communale de Jauche;
7.3. Ratification de la d�cision prise par le Coll�ge communal en sa s�ance du 23 mars 2015 et ayant trait au remplacement d'une institutrice maternelle � l'�cole communale de Jauche;
7.4. Ratification de la d�cision prise par le Coll�ge communal en sa s�ance du 30 mars 2015 et ayant trait au remplacement d'une institutrice primaire � l'�cole communale de JAUCHE