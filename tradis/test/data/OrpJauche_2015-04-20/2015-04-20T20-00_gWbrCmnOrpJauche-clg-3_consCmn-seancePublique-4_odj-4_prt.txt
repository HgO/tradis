
          - "Orp-Jauche, le 09 avril 2015.
          - "Madame, Monsieur, Cher(e) collègue,
          - "Conformément à l'article L1122-13 §1er du Code de la Démocratie Locale et de la Décentralisation et au Règlement d'Ordre Intérieur, nous avons l'honneur de vous inviter à participer à la séance du conseil communal qui aura lieu le lundi 20 avril 2015 à 20 heures, à la salle du Conseil communal, Place communale 6 à 1350 Orp-Jauche.
          - "L'ordre du jour de cette assemblée est reproduit en annexe.
          - "Dans l'attente du plaisir de vous rencontrer, nous vous prions d'agréer, Madame, Monsieur, l'assurance de notre considération très distinguée.

      - "Pour le Collège communal, 
      - "Par ordonnance,
        - "La Directrice générale, 
        - "Le Bourgmestre,
Sabrina SANTUCCI
Hugues GHENNE

        - "Séance publique"
        - h: 
          - "(première convocation)
          - "1. SECRETARIAT
          - li class="obj":
            - "1.1. Application du droit à interpellation du public;
            - "1.2. Lecture de la réponse du Collège à l'interpellation du public du 23 mars 2015;
            - "1.3. Approbation du procès-verbal de la séance du 23 mars 2015;
          - "2. COMPTABILITE
          - li class="obj":
            - "2.1. Tutelle spéciale d'approbation : approbation du budget de l'exercice 2015 du Centre public d'Action sociale ;
            - "2.2. Garantie bancaire en faveur de l'Association Chapitre XII Eugène Malevé pour la ligne de crédit auprès de Belfius Banque ;
            - "2.3. Octroi d'un subside de fonctionnement en faveur de l'ASBL Office du Tourisme d'Orp-Jauche pour l'exercice 2015 ;
          - "3. PLAN DE COHESION SOCIALE
          - li class="obj":
            - "3.1. Approbation des rapports d'activité et financier 2014 ;
          - "4. FABRIQUES D'EGLISE
          - li class="obj":
            - "4.1. Approbation des comptes 2014 de la Fabrique d'église Saints Martin et Adèle d'Orp-le-Grand ;
          - "5. MARCHES DE TRAVAUX
          - li class="obj":
            - "5.1. Marché de travaux ayant pour objet la réparation des malfaçons dans le cadre de la réfection de l'égouttage et de la voirie rue Pierre Renard (partie supérieure) - Décision de principe, approbation du CSCH, des conditions de marché et du mode de passation ;

        - "Séance à huis clos"
        - h:
          - "6. PERSONNEL
          - li class="obj":
            - "6.1. Agent bibliothécaire statutaire - Régularisation rétroactive par une mise à la retraite d'office au 01.05.2010, une modification du statut de définitif en contractuel au 01.10.2011 et une décision de fin de fonction d'agent contractuel pensionné au 30.04.2015;
          - "7. ENSEIGNEMENT
          - li class="obj":
            - "7.1. Nominations à titre définitif au 1er avril 2015 ;
            - "7.2. Ratification de la décision prise par le Collège communal en sa séance du 16 mars 2015 et ayant trait au remplacement d'une institutrice maternelle à l'école communale de Jauche;
            - "7.3. Ratification de la décision prise par le Collège communal en sa séance du 23 mars 2015 et ayant trait au remplacement d'une institutrice maternelle à l'école communale de Jauche;
            - "7.4. Ratification de la décision prise par le Collège communal en sa séance du 30 mars 2015 et ayant trait au remplacement d'une institutrice primaire à l'école communale de JAUCHE
