
PROVINCE de LUXEMBOURG ARRONDISSEMENT DE VIRTON 
          - "COMMUNE DE HABAY
          - "CONVOCATION
          - "ASSEMBLEE DU CONSEIL COMMUNAL
          - "Conformément à l'article L1122-13 du Code de la Démocratie locale et de la Décentralisation, le Collège communal a l'honneur de vous convoquer, pour la première fois, à la séance du CONSEIL COMMUNAL qui aura lieu en la Maison Communale, à HABAY-la-NEUVE, le 22 avril 2015 à 20 heures.
Ordre du Jour :
          - "(1) 04509260 (2)
          - "(3) 04508951 (4)
04509366 (5)
04509263 (6)
04508950 (7)
04510373 (8)
04509259 (9)
04508953 (10)
04509871 (11)
04508954 (12)
          - "(13) 04509868 (14)
Examen et approbation du procès-verbal de la séance du 23 mars 2015
Forêt domaniale indivise dANLIER - criasse à licence - cahier des charges : approbation Comptes communaux - exercice 2014: examen et approbation
Fabriques d'Eglises de MARBEHAN - HABAY-la-NEUVE - ORSINFAING - RULLES - HOUDEMONT comptes 2014 : examen et approbation
Octroi de divers subsides (Ecole de HOUDEMONT, Carnaval de la Marquise,
Eklektik Guys, Comité Local d'Animation de HOUDEMON, Commune de LEGLISE)
Aménagement d'un espace multisports à MARBEHAN dans le cadre du programme \"\"Sport de rue\"\" : approbation du cahier spécial des charges et choix du mode de passation du marché, fixation de la composition du comité d'accom pagnement
Parc du Châtelet à HABAY-la-NEUVE - mission d'étude préliminaire et d'accompagnement pour l'installation d'une centrale hydroélectrique approbation du cahier spécial des charges et choix du mode de passation du marché
Parc du Châtelet, à HABAY-la-NEUVE - réseau de chaleur - désignation d'un auteur de projet : approbation du cahier spécial des charges et choix du mode de passation du marché
Protection des captages - zones de prévention : approbation des dossiers Galerie du Châtelet, à HABAY-la-NEUVE - renon au bail par Mr TONGLET : approbation
Mise en concession de la Galerie du Châtelet, à HABAY-la-NEUVE : fixation des conditions
Constitution de la Conférence Luxembourgeoise des Elus en ASBL : approbation des statuts
Conseil Consultatif Provincial des Aînés - candidature de Mr Louis BASTIN : approbation Remplacement de Mlle Aline QUOIRIN au sein de différentes A.S.B.L. communales

        - "Séance à huis clos
        - li class="obj":
 04509367 (15)
04509870 (16)
04509869 (17)
Personnel enseignant : nomination d'un Maître spécial de morale non confessionnelle à raisons de deux périodes Ratification des délibérations du Collège communal portant désignation de personnel enseignant temporaire
Démission de ses fonctions pour départ à la retraite présentée par un ouvrier communal rgmestre/^.
