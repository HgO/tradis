
CALENDRIER ET ORDRE DU JOUR
          - "La prochaine séance du Conseil communal aura lieu le lundi 29 septembre 2014 à 20h à la salle du Conseil, Grand-Place Baudouin Ier, 3 (2e étage) à 1420 Braine-l'Alleud. Bienvenue à tous !

        - "Séance publique
        - h class="obj":
          - "1. ÉGLISE REFORMEE DE L'ALLIANCE - BUDGET 2015
          - p:
            - "
Le Conseil est invité à émettre un avis favorable au sujet le budget 2015 introduit par l'Eglise Réformée de l'Alliance.
          - "2. EGLISE PROTESTANTE EVANGELIQUE - BUDGET 2015
          - p:
            - "
Le Conseil est invité à émettre un avis favorable au sujet du budget 2015 introduit par l'Eglise protestante évangélique.
          - "3. FABRIQUE D'EGLISE SAINTE-GERTRUDE - BUDGET 2015
          - p:
            - "
Le Conseil est invité à émettre un avis favorable au sujet du budget 2015 de la Fabrique d'église Sainte-Gertrude.
          - "4. PROCES-VERBAL DE VERIFICATION DE LA CAISSE COMMUNALE AU 30.06.2014
          - p:
            - "
Le Conseil communal est invité à prendre connaissance du procès-verbal de vérification de la caisse communale établi au 30.06.2014 par Mme S. MARCOUX, Echevine des Finances.
          - "5. CENTIMES ADDITIONNELS A LA TAXE REGIONALE SUR LES MATS, PYLONES ET ANTENNES - EXERCICE 2014
          - p:
            - "
Par le décret du 11.12.2013, la taxe sur les mâts, pylônes et antennes est devenue une matière régionale. Les communes ne peuvent donc plus appliquer cette taxe. Il leur est néanmoins permis de lever des centimes additionnels. Le Conseil est dès lors invité à établir, pour l'exercice 2014, une taxe additionnelle à la taxe régionale sur les mâts, pylônes ou antennes.
          - "6. ENSEIGNEMENT PRIMAIRE - LILLOIS - EQUIPEMENT ET MAINTENANCE EXTRAORDINAIRE EN COURS D’EXECUTION DES BATIMENTS – AMELIORATION DU SYSTEME DE CHAUFFAGE - REMPLACEMENT DES CHAUDIERES ET MAINTENANCE - PROJET - DEVIS - MODE DE MARCHE
          - p:
            - "
Il est proposé au Conseil communal de marquer son accord de principe sur le remplacement des chaudières de l’école communale de Lillois dont le devis estimatif de la dépense est arrêté à la somme de 123.000,00 € T.V.A. 21 % comprise et d’autoriser le Collège communal à attribuer le marché par la procédure d’appel d’offres ouvert.
          - "7. TRAVAUX DE CURAGE, D’ENTRETIEN ET DE REPARATION AUX COURS D’EAU NON NAVIGABLES DES 2e ET 3e CATEGORIES DE LA COMMUNE – AVENANT N° 2 – APPROBATION
          - p:
            - "
Il est proposé au Conseil communal de prendre connaissance de la délibération du 25.08.2014 par laquelle le Collège communal, en raison de l’urgence, marque son accord sur l’avenant n° 2 au marché de curage, d’entretien et de réparation aux cours d’eau non navigables des 2e et 3e catégories de la Commune, arrêté à la somme de 53.119,00 € T.V.A. 21 % comprise.
          - "8. PATRIMOINE PRIVE – EQUIPEMENT ET MAINTENANCE EXTRAORDINAIRE EN COURS D’EXECUTION DES BATIMENTS – AVENUE LÉON JOUREZ N° 37 - REPARATION DE LA TOITURE - PROJET – DEVIS – MODE DE MARCHE
          - p:
            - "
Il est proposé au Conseil communal de marquer son accord de principe sur les travaux de réfection de la toiture du bâtiment sis 37 avenue Léon Jourez dont le devis estimatif de la dépense est arrêté à la somme de 14.798,30 € T.V.A. 21 % comprise et d’autoriser le Collège communal à attribuer le marché par la procédure négociée sans publicité.
          - "9. INSTALLATIONS SPORTIVES - EQUIPEMENT, MAINTENANCE EXTRAORDINAIRE ET INVESTISSEMENT SUR TERRAIN EN COURS D'EXECUTION - STADE GASTON REIFF - REMPLACEMENT DE LA CLOTURE – PROJET – DEVIS – MODE DE MARCHE
          - p:
            - "
Il est proposé au Conseil communal de marquer son accord de principe sur les travaux de remplacement d’une clôture du stade Gaston Reiff dont le devis estimatif de la dépense est arrêté à la somme de 9.997,02 € T.V.A. 21 % comprise et d’autoriser le Collège communal à attribuer le marché par la procédure négociée sans publicité.
          - "10. ZONE DE POLICE DE BRAINE-L'ALLEUD 5273 - ACQUISITION D'UN VEHICULE ET ACCESSOIRES - LOT 1 - DECISION DE RECOURIR AUX MARCHES DE LA POLICE FEDERALE - LOTS 2 A 4 - PRINCIPE - DEVIS - MODE DE MARCHE
          - p:
            - "
Il est proposé au Conseil communal de marquer son accord de principe sur la fourniture d’un véhicule avec les accessoires \" Police \" pour la zone de Police de Braine-l’Alleud n° 5273 dont les devis estimatifs de la dépense sont arrêtés à la somme globale de 40.782,25 € T.V.A. 21 % comprise, d’autoriser le Collège communal à attribuer le marché la procédure négociée par facture acceptée pour les lots 2 à 4 et à passer commande auprès du fournisseur désigné pour le marché de la Police fédérale pour le lot 1.
          - "11. ADHESION A LA CENTRALE D'ACHAT ET / OU DE MARCHE DU GIAL - CONVENTION
          - p:
            - "
Le Conseil communal est appelé à approuver le projet de convention proposé par l'a.s.b.l. Gial qui poursuit des activités de centrales d'achat et de marché au bénéfice d'administrations publiques ou d'entités adjudicatrices non membres. L'Intercommunale IMIO à laquelle nous avons fait appel pour obtenir une aide dans la configuration du réseau informatique à installer au nouveau centre administratif de l’avenue du Japon, nous propose d'adhérer à cette centrale d'achat et/ou de marché.
          - "12. PROCES-VERBAL DE LA SEANCE PUBLIQUE DU 01.09.2014
          - p:
            - "
          - "13. QUESTIONS DIVERSES (ARTICLE 79 DU REGLEMENT D'ORDRE INTERIEUR)
          - p:
            - "
