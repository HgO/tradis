Aywaille, le 14 avril 2015.
Administration Communale 4920 AYWAILLE
Drapeau d'honneur de l'Europe    1986
Aux membres du Conseil communal De et � 4920 Aywaille
Plaquette d'honneur de l'Europe 2002 _
B.C.E. : 0.207.338.686 Service : Secr�tariat
Conform�ment aux articles L1122-12 (NLC 86) et L1122-14 (NLC 87 bis) de la Loi communale, nous avons l'honneur de vous convoquer � la s�ance du Conseil communal qui aura lieu le
Les membres du Conseil sont inform�s que les dossiers peuvent �tre consult�s au secr�tariat du lundi au vendredi de 9 � 12h00 et de 13 � 16h00 et le lundi 20 avril 2015 de 18 � 20h00 sur rendez-vous.
S�ance publique.
Communications du Coll�ge communal sur l'�tat d'avancement de diff�rents dossiers. Proc�s-verbal de la s�ance du 26 mars 2015 - Approbation.
1. Fabrique d'Eglise Saint-Joseph � Deign� - Compte 2014- Approbation.
2. Nettoyage des locaux de diff�rents b�timents communaux - Approbation des conditions et du mode de passation - D�cision.
3. CRIPEL - Convention de partenariat - Approbation.
4. Fonctionnaire sanctionnateur- Convention avec la Province.
5. Voiries - D�classement d'une partie du chemin n� 114, sis au lieu-dit � Heid d'isle � � 4920 Sougn�-Remouchamps - D�cision.
6. Acquisition de modules de jeux pour l'espace de quartier Sur la Heid et � Havelange - Approbation des conditions et du mode de passation - D�cision.
7. Acquisition d'une cam�ra-imagerie thermique infrarouge - Approbation des conditions et du mode de passation - D�cision.
8. Acquisition de poubelles publiques - Approbation des conditions et du mode de passation du march� - D�cision.
9. Acquisition d'une pilonneuse pour le service de voirie - Approbation des conditions et du mode de passation du march� - D�cision.
10. Acquisition et mise en place d'un columbarium pour le cimeti�re de Dieupart - Approbation des conditions et du mode de passation du march� - D�cision.
11. Acquisition de mat�riel informatique - Ecoles de Kin et de Sougn�-Remouchamps - Approbation des conditions et du mode de passation du march� - D�cision.
12. Organisation des actions de pr�vention et de sensibilisation pour l'ann�e 2015 par l'intercommunale INTRADEL - D�cision.
13. SRI - Personnel - Appel � promotion pour les grades d'Adjudant, Sergent et Caporal au SRI d'Aywaille - D�cision.
14. SRI - Transfert � la zone de secours des emprunts contract�s parla commune et relatifs � des biens transf�r�s � la zone de secours - D�cision.
15. Redevance incendie 2012 - Quotes-parts des Centres de Groupe r�gionaux - Avis.
16. SWDE - Assembl�e G�n�rale Ordinaire du 26/05/2015 - Mandat de vote.
17. Ordonnances de police - Prise d'acte - Ratification.
18. Motion de soutien aux installations militaires sur le territoire de la Province de Li�ge - Adoption.
MERCREDI 22 avril 2015 � 20 heures � l'H�tel de Ville.
19. Propositions de motion : projet de Partenariat transatlantique sur le Commerce et l'Investissement entre l'Union europ�enne et les Etats-Unis d'Am�rique - Adoption.
Questions orales des Conseillers au Coll�ge communal.
Huis clos
1. Personnel communal - Mise en disponibilit� pour maladie - D�cision.
2. Personnel enseignant - D�signations temporaires - Confirmation.
Administration communale Rue de la Heid 8 � 4920 Aywaille. G) 04.384.40.17 - i 04.384.77.92 -    info@aywaille.be - H www.aywaille.be Agent traitant : Dominique Sadzot- Employ�e d'administration G) 04.364.05.46 - O3 dominique.sadzot@aywaille.be Ouverture au public du lundi au vendredi : de 9h00 � 12h00 et de 13h30 � 15h30 (fermeture du service Urbanisme le jeudi apr�s-midi). Permanence �tat civil, population & urbanisme : le samedi, de 9h00 � 12h00 (pour le service Urbanisme, sur rendez-vous uniquement) ; le mardi, jusqu'�-19h00.
Par le Coll�ge :
Page 2 sur 2