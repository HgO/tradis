====== Le 2012-10-29 à 20h : Séance publique du Conseil communal de Braine l'Alleud ======
  * Commune : Braine l'Alleud
  * Titre : Séance du conseil communal
  * Date : 2012-10-29T20-00
  * Lieu : salle du conseil, Grand'Place Baudouin Ier, 3 (2e étage)

----


La prochaine séance du Conseil communal aura lieu le lundi 29 octobre à 20h dans la salle du conseil, Grand'Place Baudouin Ier, 3 (2e étage).

====== Ordre du jour ======
  * **1** Secrétariat communal - Décisions des autorités de tutelle – Information.

Le Collège communal informe le Conseil des décisions prises par les autorités de tutelle.

  * **2** Secrétariat communal - Intercommunale Bataille de Waterloo 1815 S.C.R.L. – Assemblée générale du 13 novembre 2012.

Il est proposé aux membres du Conseil communal de se prononcer sur les points inscrits à l'ordre du jour de l’Assemblée générale du 13 novembre 2012 de l’Intercommunale Bataille de Waterloo 1815 s.c.r.l. qui requièrent une décision du Conseil communal, à savoir :

    * Approbation du PV de l'A.G. ordinaire du 12 juin 2012
    * Approbation du budget 2013 tel qu'approuvé en CA
    * Approbation rapport d'évaluation du plan stratégique 2011-2013

  * **3** Finances communales – Budget communal pour 2012 - Modification budgétaire n°3 des services ordinaire et extraordinaire.

Le Conseil communal est invité à arrêter la 3e modification budgétaire des services ordinaire et extraordinaire de l’exercice 2012.

  * **4** Finances communales – Subsides 2012 – Complément.

Le Conseil communal est invité à marquer son accord sur l’octroi d’un subside complémentaire de 500 euros sollicité par l’asbl Chœur la Noucelle.

  * **5** Finances communales – Fabrique d’église Notre Dame du Bon Conseil – Compte 2011.

Le Conseil communal est invité à émettre un avis favorable au sujet du compte 2011 introduit par la Fabrique d’église Notre Dame du Bon Conseil.

  * **6** Finances communales – Fabrique d’église Saint Etienne – Budget 2012 – Modification budgétaire n°1.

Le Conseil communal est invité à émettre un avis favorable au sujet de la modification budgétaire n°1 de l’exercice 2012 introduite par la Fabrique d’église Saint Etienne.

  * **7** Finances communales – Fabrique d’église du Sacré-Cœur – Budget 2013.

Le Conseil communal est invité à émettre un avis favorable au sujet du budget introduit par la Fabrique d’église du Sacré-Cœur pour l’exercice 2013

  * **8** Finances communales – Fabrique d’église Notre Dame du Bon Conseil – Budget 2013.

Le Conseil communal est invité à émettre un avis favorable au sujet du budget introduit par la Fabrique d’église Notre Dame du Bon Conseil pour l’exercice 2013.

  * **9** Finances communales – Fabrique d’église Saint Etienne – Budget 2013.

Le Conseil communal est invité à émettre un avis favorable au sujet du budget introduit par la Fabrique d’église Saint Etienne pour l’exercice 2013.

  * **10** Finances communales – Eglise protestante évangélique – Budget 2013.

Le Conseil communal est invité à émettre un avis défavorable au sujet du budget introduit par l’Eglise protestante évangélique pour l’exercice 2013.

  * **11** Finances communales – Fabrique d’église Sainte Gertrude – Budget 2013.

Le Conseil communal est invité à émettre un avis favorable au sujet du budget introduit par la Fabrique d’église Sainte Gertrude pour l’exercice 2013.

  * **12** Finances communales – Fabrique d’église Saint Etienne – Travaux de rénovation de l’électricité – Financement – Caution - Accord de principe.

Le Conseil communal est invité à marquer son accord de principe de garantir le financement (caution) des travaux de rénovation de l’électricité de l’église non subsidiés par le Service public Wallonie.

  * **13** Finances communales – Taxe sur la distribution gratuite à domicile d’écrits publicitaires non adressés – Exercice 2013.

Le Conseil communal est invité à voter le règlement-taxe sur la distribution gratuite à domicile d’écrits publicitaires non adressés pour l’exercice 2013.

  * **14** Finances communales – Taxe sur les sablières – Exercice 2013.

Le Conseil communal est invité à voter le règlement-taxe sur les sablières pour l’exercice 2013.

  * **15** Finances communales – Taxe sur la délivrance de documents administratifs – Exercice 2013.

Le Conseil communal est invité à voter le règlement-taxe sur la délivrance de documents administratifs pour l’exercice 2013.

  * **16** Finances communales – Taxe sur l’enlèvement des déchets ménagers et des déchets y assimilés – Exercice 2013.

Le conseil communal est invité à voter le règlement-taxe sur l’enlèvement des déchets ménagers et des déchets ménagers y assimilés pour l’exercice 2013.

  * **17** Finances communales – Taxe additionnelle au précompte immobilier – Exercice 2013.

Le Conseil communal est invité à voter le taux de la taxe additionnelle au précompte immobilier pour l’exercice 2013.

  * **18** Finances communales – Taxe additionnelle à l’impôt des personnes physiques – Exercice 2013.

Le Conseil communal est invité à voter le taux de la taxe additionnelle à l’impôt des personnes physiques pour l’exercice 2013.

  * **19** Finances communales – Zone de police n°5273 – Procès-verbal de vérification de la caisse au 30.09.2012.

Le Conseil communal est invité à prendre connaissance du procès-verbal de la vérification de la caisse au 30.09.2012 de la zone de police.

  * **20** Finances communales – Zone de police n°5273 – Budget 2012 – Modification budgétaire n°2 du service ordinaire.

Le Conseil communal est invité à arrêter la modification budgétaire n°2 du service ordinaire de l’exercice 2012.

  * **21** Marchés publics – Bibliothèque centrale – Aménagement d’une salle de lecture – Principe - Lots 1 et 2 – Projet - Devis – Mode de marché – Subsides.

Le Conseil communal est invité à marqué son accord de principe sur l’aménagement de la salle de lecture de la Bibliothèque centrale dont le montant estimatif de la dépense est fixé à 9.999,44 € T.V.A. comprise.

  * **22** Marchés publics - Budget extraordinaire 2012 – 3e modification budgétaire - Mode de passation et conditions de certains marchés.

Le Conseil communal est invité à arrêter la liste des marchés de faible importance et dont les dépenses sont inscrites au budget extraordinaire de l’exercice 2012 et à autoriser le Collège communal à attribuer ces marchés par la procédure négociée sans publicité.

  * **23** Marchés publics - Centres culturels et de loisirs – Achats de matériel d’équipement et d’exploitation – Centre culturel - Acquisition de projecteurs - Projet – Devis – Mode de marché.

Le Conseil communal est invité à marqué son accord de principe sur l’acquisition de projecteurs pour le Centre culturel dont le montant estimatif de la dépense est fixé à 9.680 euros T.V.A. comprise.

  * **24** Marchés publics - Cimetières – Equipement, maintenance extraordinaire et investissements sur terrains en cours d’exécution – Cimetière du Centre – Petites restaurations de monuments - Principe - Devis - Mode de marché.

Le Conseil communal est invité à marqué son accord de principe sur la réalisation de petits travaux de restauration de monuments funéraires au cimetière du Centre dont le montant estimatif de la dépense est fixé à 6.640,48 euros T.V.A. comprise.

  * **25** Marchés publics - Divers services communaux – Achat de matériel informatique – Principe – Lots 1 à 9 – Projet – Devis – Mode de marché.

Il est proposé au Conseil communal de marquer son accord de principe sur l’acquisition de divers équipements pour différents services communaux dont le montant estimatif global de la dépense est fixé à 29.664,36 T.V.A. comprise.

  * **26** Marchés publics - Enseignements gardien et primaire – Equipement et maintenance extraordinaire en cours d’exécution des bâtiments – Ecole communale d’Ophain - Equipement de cuisine et aménagement du self-service – Principe – Projet – Devis – Mode de marché.

Le Conseil communal est invité à marquer son accord de principe sur l’acquisition d’un équipement de cuisine et l’aménagement d’un self-service pour l’école communale d’Ophain dont le montant estimatif de la dépense est fixé à 24.926 euros T.V.A. comprise.

  * **27** Marchés publics - Enseignements gardien et primaire – Achat de matériel d’exploitation – Ecole communale d’Ophain - Matériel de cuisine – Principe – Projet – Devis – Mode de marché.

Le Conseil communal est invité à marquer son accord de principe sur l’acquisition de matériel de cuisine pour l’école communale d’Ophain dont le montant estimatif de la dépense est fixé à 9.990,24 euros T.V.A. comprise.

  * **28** Marchés publics - Enseignement primaire Lillois - Aménagements aux terrains en cours d'exécution - Extension de l'école de Lillois - Projet – Devis – Mode de marché – Tutelle – Subsides.

Le Conseil communal est invité à marquer son accord de principe sur la réalisation de travaux de construction de nouveaux locaux à l’école communale de Lillois dont le montant estimatif de la dépense est fixé à 1.545.521,76 euros T.V.A. comprise.

  * **29** Marchés publics - Hydraulique – Aménagements aux terrains en cours d’exécution – Création d’une zone d’immersion temporaire (Z.I.T.) sur le site dit " du Paradis " – Avenant n°1.

Le Conseil communal est invité à approuver l’avenant n°1 relatif à l’exécution de travaux supplémentaires pour un montant de 486.212,36 euros T.V.A. comprise, dans le cadre du marché de création de la zone d’immersion temporaire sur le site dit « du Paradis ». il est également proposé au Conseil de marquer son accord sur la prolongation du délai d’exécution de 60 jours ouvrables.

  * **30** Marchés publics - Eaux usées – Travaux de construction d’infrastructures en cours d’exécution – Egouttage – Création d’une digue rue Les Culots – Projet – Devis – Mode de marché.

afin de lutter efficacement contre les inondations à répétition, il est proposé au Conseil communal de marquer son accord de principe sur la réalisation d’une digue rue Les Culots dont le montant estimatif de la dépense est fixé à 29.524 euros T.V.A. comprise. Cette digue devant être construite sur terrain privé, le Conseil est également invité à approuver la convention à passer avec le propriétaire dudit terrain.

  * **31** Marchés publics – Administration générale – Achat de machines et de matériel d’équipement – Acquisition et installation d’une caméra de surveillance sur la place située à l’arrière de l’hôtel communal – Projet – Devis – Mode de marché.

Afin de sécuriser la place située à l’arrière de l’hôtel communal, il est proposé au Conseil communal de marquer son accord de principe sur l’acquisition et l’installation d’une caméra de surveillance dont le montant estimatif de la dépense est fixé à 9.982,50 euros T.V.A. comprise.

  * **32** Marchés publics - Service Incendie – Maintenance extraordinaire des camions – Réparation du bras élévateur – Projet – Devis – Mode de marché.

Le Conseil communal est invité à admettre la dépense relative à la réparation du bras élévateur d’un véhicule du service Incendie dont le montant est fixé à 8.470 euros T.V.A. comprise.

  * **33** Marchés publics - Zone de police de Braine-l'Alleud n° 5273 - Achat de matériel divers - Acquisition d’une armoire à clés avec ouverture électronique – Projet – Devis – Mode de marché.

Pour des raisons de confidentialité et de sécurité, il est proposé au Conseil communal de marquer son accord de principe sur l’acquisition d’une armoire à clés avec ouverture électronique pour la zone de police dont le montant estimatif de la dépense est fixé à 10.000 euros T.V.A. comprise.

  * **34** Marchés publics - Zone de Police de Braine-l'Alleud n° 5273 – Achat de matériel informatique – Logiciel procès-verbal - Projet - Devis - Mode de marché.

Il est proposé au Conseil communal de marquer son accord de principe sur l’acquisition d’une application informatique permettant la rédaction automatique des procès-verbaux de roulage dont le montant estimatif de la dépense est fixé à 17.000 euros T.V.A. comprise.

  * **35** Marchés publics - Service salubrité – Maintenance extraordinaire des véhicules spéciaux et divers – Réparation de la balayeuse – Projet – Devis – Mode de marché.

Il est proposé au Conseil communal d’admettre la dépense relative à la réparation de la balayeuse du service Salubrité dont le montant est fixé à 10.890,00 euros T.V.A. comprise.

  * **36** Marchés publics - Voiries – Entretien extraordinaire de la voirie et de l’infrastructure en cours d’exécution – Travaux de réaménagement de la rue Joseph Gos - Projet – Devis – Mode de marché.

Il est proposé au Conseil communal de marquer son accord de principe sur la réalisation des travaux de réaménagement de la rue Joseph Gos dont le montant estimatif de la dépense est fixé à 287.291,99 euros T.V.A. comprise.

  * **37** Marchés publics - Lutte contre les inondations – Achat de matériel – Achat de matériel d’intervention d’urgence - Projet – Devis – Mode de marché.

Dans le cadre du plan de lutte contre les inondations, il est proposé au Conseil communal de marquer son accord de principe sur l’acquisition de matériel d’intervention d’urgence dont le montant estimatif de la dépense est fixé à 11.966,90 euros T.V.A. comprise.

  * **38** Marchés publics - Voirie - Entretien extraordinaire de la voirie et de l'infrastructure en cours d'exécution – Réfection de la voirie - Chemin du Bois de Hal – Projet – Devis – Mode de marché.

Il est proposé au Conseil communal de marquer son accord de principe sur l’aménagement du carrefour formé par la chaussée de Tubize et le chemin du Bois de Hal dont le montant estimatif de la dépense est fixé à 67.816,87 euros.

  * **39** Travaux publics – Eclairage public – Remplacement d’armatures vétustes à l’avenue du Dernier Carré – Principe – Devis.

Il est proposé au Conseil communal d’admettre le principe de l’exécution des travaux de remplacement des armatures vétustes du système d’éclairage public à l’avenue du Dernier Carré dont le montant estimatif de la dépense est fixé à 9.565,88 euros T.V.A. comprise.

  * **40** Travaux publics - Eclairage public – Aménagement d’un éclairage de sécurité aux passages pour piétons à la chaussée de Mont-Saint-Jean – Principe – Devis.

Il est proposé au Conseil communal d’admettre le principe de l’exécution des travaux de renforcement de l’éclairage public de sécurité au niveau des deux passages pour piétons situés au croisement du chemin des Roussettes et de la chaussée de Mont-Saint-Jean dont le montant estimatif de la dépense est fixé à 8.104,00 euros T.V.A. comprise.

  * **41** Travaux publics - Construction de la station de pompage au quartier de la Justice - Emprises et zones de travail dans les propriétés communales.

En vue de l’exécution des travaux de réalisation d’une station de pompage au quartier de la Justice et de l’établissement d’un collecteur dans les propriétés communales situées entre la rue des Noisetiers et la rue des Champs clairs, il est proposé au Conseil communal de céder à l’I.B.W. les emprises en pleine propriété et en sous-sol ainsi qu’une zone de travail nécessaires.

  * **42** Travaux publics - Zone de police de Braine-l’Alleud n° 5273 - Budget extraordinaire 2012 - Mode de passation et conditions de certains marchés.

Il est proposé au Conseil communal d’autoriser le Collège communal à attribuer, par la procédure négociée sans publicité, le marché relatif à l’acquisition de matériel ralentisseur de trafic pour la zone de police dont le montant estimatif de la dépense est fixé à 3.500 euros.

  * **43** Travaux publics - Zone de Police de Braine-l’Alleud n° 5273 – Acquisition de matériel informatique – Lot 1 : décision de recourir aux marchés de l’Etat fédéral – Lot 2 : principe – Devis – Mode de marché.

Il est proposé au Conseil communal de marquer son accord de principe sur l’acquisition, via les marchés de l’Etat fédéral, de matériel informatique pour la zone de police dont le montant global estimatif de la dépense est fixé à 9.993,79 euros T.V.A. comprise.

  * **44** Travaux publics - Zone de police de Braine-l’Alleud n° 5273 – Acquisition de tenues de maintien de l’ordre – Décision de recourir aux marchés de la police fédérale.

Il est proposé au Conseil communal de marquer son accord de principe sur l’acquisition, via les marchés de l’Etat fédéral, d’équipements individuels pour le maintien de l’ordre public pour la zone de police dont le montant estimatif de la dépense est fixé à 14.089,19 euros T.V.A. comprise.

  * **45** Travaux publics - Service Incendie – Programme d’acquisition de matériel avec subsides de l’État fédéral – Période 2002-2012 – Complément.

Il est proposé au Conseil communal de compléter la programme d’acquisition pluriannuel du matériel d’incendie par l’achat de radios portatives dont le montant de la part communale est estimé à 2.105 euros.

  * **46** Travaux publics – Sedilec – Servitude de passage avec emprise en sous-sol rue des Berges du Ruisseau – Alimentation en éclairage public de la nouvelle voirie « chemin du Fond Saint-Georges ».

Dans le cadre de la pose de câbles servant à alimenter en éclairage public la nouvelle voirie située le long du chemin de fer et dénommée chemin du Fond Saint Georges, il est proposé au Conseil communal de concéder à l’intercommunale Sedilec une servitude de passage avec emprise en sous-sol dans la parcelle communale située rue des Berges du Ruisseau.

  * **47** Travaux publics - Réalisation d’un centre aquatique – Acquisition de terrains – Convention transactionnelle - Projet d’acte.

Il est proposé au Conseil communal de ratifier la convention signée du 09.07.2012 et d’acquérir, pour cause d’utilité publique, au prix de 259.000 euros, les parcelles ou parties de parcelles en vue de la construction du centre aquatique.

  * **48** Travaux publics – Financement alternatif-Acquisition d’un immeuble de bureaux en vue du regroupement des services communaux de Braine-l’Alleud- Principe.

Le Conseil communal est invité à marquer un accord de principe au sujet de l’acquisition, pour cause d’utilité publique, du bâtiment situé 51, avenue du Japon à Braine-l’Alleud afin d’y regrouper l’ensemble des services communaux.

  * **49** Zone de police de Braine-l’Alleud n° 5273 – Déclaration de la vacance d’emplois au cadre opérationnel – Mobilité 2012-05.

Il est proposé au Conseil communal de déclarer la vacance de 5 emplois d’inspecteur de police au département opérationnel de la zone de police de Braine-l’Alleud dans le cadre de la mobilité 2012-05.

  * **50** Régie foncière et immobilière – Budget 2013.

Il est proposé au Conseil communal d’arrêter le budget de la Régie foncière et immobilière pour l’exercice 2013.

  * **51** Questions diverses (Article 81 du Règlement d’Ordre intérieur).
  * **52** Procès-verbal de la séance publique du 27.08.2012.

----

  * Source :
    * Date : 2012-10-24T00-00
    * Adresse : http://www.braine-lalleud.be/fr/Administration/conseil/calendrier-et-ordre-du-jour.html
    * Collecteur : Installé Patrick
    * Droit d'auteur : CC BY-NC-SA 2.0 (http://creativecommons.org/licenses/by-nc-sa/2.0/be/)

