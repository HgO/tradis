Calendrier et ordre du jour
La prochaine séance du Conseil communal se tiendra le lundi 28 octobre à 20h. Elle se déroulera dans la salle du conseil, Grand'Place Baudouin Ier, 3 (2e étage) à 1420 Braine-l'Alleud. Bienvenue à tous !
Ordre du jour
1. Affaires générales – Lauréats du Travail – Remise des brevets – Promotions 2012 et 2013.
Le Président du Conseil remettra à trois habitants de Braine-l'Alleud les diplômes de Lauréat du Travail.
2. Secrétariat communal - Décisions des autorités de tutelle – Information.
Conformément à l'article 4 de l'arrêté du Gouvernement wallon du 05.07.2007, le Collège communal informe le Conseil des décisions des autorités de tutelle.
3. Secrétariat communal – Intercommunale du Brabant wallon (I.B.W.) – Désignation des délégués au sein de l'assemblée générale – Modification.
Le Conseil communal est invité à acter la démission de Madame T. SNOY et d'OPPUERS en qualité de déléguée au sein de l'assemblée générale de l'I.B.W. et de désigner Monsieur R. MASSART afin de pourvoir à son remplacement.
4. Secrétariat communal – Gestion des délibérations du Collège et du Conseil – Adhésion à l'Intercommunale de Mutualisation en matière Informatique et Organisationnelle (IMIO) – Souscription de parts A – Désignation des délégués et candidature d'un(e) adminstrateur(trice).
L'intercommunale IMIO a pour but de promouvoir et coordonner la mutualisation de solutions organisationnelles, de produits et de services informatiques pour les pouvoirs locaux.
Il est dès lors proposé au Conseil communal :
 de devenir membre de l'intercommunale IMIO de souscrire 100 parts A en capital social par un apport en numéraire de 1.855 euros de proposer la candidature d'un(e) administrateur(trice) au sein du conseil d'administration de désigner 5 délégués communaux au sein de l'assemblée générale selon la clé D'Hondt.
5. Affaires générales – Règlement complémentaire de roulage – Avenue des Octrois, 11 – Stationnement réservé aux personnes handicapées.
Le Conseil communal est invité à arrêter un règlement complémentaire de roulage visant à créer un emplacement de parking réservé aux personnes handicapées, avenue des Octrois, au niveau du premier emplacement de stationnement du petit parking situé à côté du n°11.
6. Affaires générales – Règlement complémentaire de roulage – Avenue de Menden (accès école Riva-Bella) – Stationnement réservé aux personnes handicapées.
Le Conseil communal est invité à arrêter un règlement complémentaire de roulage visant à créer un emplacement de parking réservé aux personnes handicapées, avenue de Menden, au premier emplacement de stationnement à côté de la grille d'accès à l'école.
7. Affaires générales – Règlement complémentaire de roulage – Place de la Gare – Stationnement réservé aux personnes handicapées.
Le Conseil communal est invité à arrêter un règlement complémentaire de roulage visant à créer un emplacement de parking réservé aux personnes handicapées, place de la Gare, au premier emplacement de la zone de chargement et déchargement établie devant la gare.
8. Affaires générales – Règlement complémentaire de roulage – Grand'Route, 133 – Stationnement réservé aux personnes handicapées.
Le Conseil communal est invité à marquer son accord sur le projet de règlement complémentaire de roulage présenté par le Service public de Wallonie, visant à créer un emplacement de stationnement pour personnes handicapées à hauteur du n°133 de la Grand'Route à Lillois.
9. Enfance et Jeunesse – Plan de cohésion sociale (P.C.S.) – Appel à projet du plan de cohésion sociale 2014-2019 – Formulaire – Approbation.
Le Conseil communal est invité à approuver le nouveau Plan de cohésion sociale. Il se décline en 31 actions coordonnées au sein de 4 axes : l'insertion socioprofessionnelle, l'accès à un logement décent, l'accès à la santé et le traitement des assuétudes et le retissage des liens sociaux, intergénérationnels et interculturels.
10. Service du Personnel – Centre psycho-médico-social de la Province du Brabant wallon – Convention d'affiliation – Avenant n°1.
Les établissements scolaires sont affiliés au Service provincial de Promotion de la Santé à l'École du Brabant wallon, celui-ci mettant à disposition gratuitement des médecins et du personnel infirmier pour assurer les bilans de santé. Le 31.01.2008, une convention a été signée entre la Commune et la Province sur base du modèle fixé par l'arrêté du Gouvernement de la Communauté française. La Province propose cependant d'y préciser la tacite reconduction et l'adresse du centre de santé de Nivelles par la conclusion d'un avenant n°1.
11. Zone de police de Braine-l'Alleud n°5273 – Déclaration de la vacance d'emplois au cadre opérationnel – Mobilité 2013-05.
Le Conseil communal est invité à déclarer la vacance :
 de 5 emplois d'inspecteur de police au département opérationnel de la zone de police (section d'interventions) d'un emploi d'inspecteur principal de police au département opérationnel de la zone de police (sections d'intervention) d'un emploi d'inspecteur principal de police au département circulation de la zone de police (gestion administrative).
Zone de police de Braine-l'Alleud n°5273 – Déclaration de la vacance d'un emploi au cadre administratif et logistique – Mobilité 2013-05.
Le Conseil communal est invité à déclarer la vacance d'un emploi statutaire de Calog niveau C à la Direction appui (service GRH) de la zone de police.
12. Finances communales - Église réformée de l'Alliance – Budget 2014.
Le Conseil communal est invité à émettre un avis favorable au sujet du budget introduit par l'Église réformée de l'Alliance pour l'exercice 2014 se clôturant en équilibre à 17.392,16 euros et prévoyant une intervention communale de 5.927,47 euros.
13. Finances communales - Fabrique d'église Notre-Dame du Bon Conseil – Budget 2014.
Le Conseil communal est invité à émettre un avis favorable au sujet du budget introduit par la Fabrique d'église Notre-Dame du Bon Conseil pour l'exercice 2014 se clôturant en équilibre à 20.310,00 euros et prévoyant une intervention communale de 5.736,91 euros.
14. Finances communales - Fabrique d'église All Saints – Budget 2014.
Le Conseil communal est invité à émettre un avis défavorable au sujet du budget introduit par l'Église All Saints pour l'exercice 2014, se clôturant en excédent à 739,95 euros et ne prévoyant aucune intervention communale.
15. Finances communales - Taxe additionnelle à l'impôt des personnes physiques pour 2014.
Le Conseil communal est invité à voter le règlement sur la taxe additionnelle à l'impôt des personnes physiques pour l'exercice 2014 (5,9 %).
16. Finances communales - Taxe additionnelle au précompte immobilier pour 2014.
Le Conseil communal est invité à voter le règlement sur la taxe additionnelle au précompte immobilier pour l'exercice 2014 (1730 centimes additionnels).
17. Finances communales - Taxe sur les sablières.
Le Conseil communal est invité à voter le règlement taxe sur les sablières pour les exercices 2014 à 2019.
18. Finances communales - Taxe sur les surfaces de bureaux.
Le Conseil communal est invité à voter le règlement taxe sur les surface de bureaux pour les exercices 2014 à 2019.
19. Finances communales - Taxe sur les terrains de golf.
Le Conseil communal est invité à voter le règlement taxe sur les terrains de golf pour les exercices 2014 à 2019.
20. Finances communales - Taxe sur les immeubles inoccupés.
Le Conseil communal est invité à voter le règlement taxe sur les immeubles inoccupés pour les exercices 2014 à 2019.
21. Finances communales - Taxe sur la construction et l'aménagement de bâtiments.
Le Conseil communal est invité à voter le règlement taxe sur la construction et l’aménagement de bâtiments pour les exercices 2014 à 2019.
22. Finances communales - Taxe sur l’enlèvement des déchets ménagers et des déchets y assimilés.
Le Conseil communal est invité à voter le règlement taxe sur l’enlèvement des déchets ménagers et des déchets y assimilés pour l’exercice 2014.
23. Finances communales - Taxe sur la délivrance de permis d’urbanisation.
Le Conseil communal est invité à voter le règlement taxe sur la délivrance de permis d’urbanisation pour les exercices 2013 à 2019.
24. Finances communales - Redevance sur l’occupation de la voie publique par des métiers forains.
Le Conseil communal est invité à voter le règlement redevance sur l’occupation de la voie publique par des métiers forains pour l’exercice 2014.
25. Finances communales - Redevance sur le droit de place sur les marchés.
Le Conseil communal est invité à voter le règlement redevance sur le droit de place sur les marchés pour les exercices 2014 et suivants.
26. Finances communales - Redevance sur l’enlèvement et l’entreposage de véhicules saisis par la police ou déplacés par mesure de police.
Le Conseil communal est invité à voter le règlement redevance sur l’enlèvement et l’entreposage de véhicules saisis par la police par mesure de police pour les exercices 2014 et suivants.
27. Finances communales - Budget communal pour 2013 – Modification budgétaire n°2 des services ordinaire et extraordinaire.
Le Conseil communal est invité à voter la modification budgétaire n°2 des services ordinaire et extraordinaire pour l’exercice 2013.
28. Finances communales - Subsides 2013 – Complément.
Le Conseil communal est invité à approuver la deuxième liste des subventions à allouer en 2013.
29. Finances communales - Zone de police n°5273 – Budget pour 2013 – Modification budgétaire n°2 des services ordinaire et extraordinaire.
Le Conseil communal est invité à arrêter la modification budgétaire n°2 des services ordinaire et extraordinaire de la zone de police.
30. Finances communales – Zone de police n°5273 – Procès-verbal de vérification de la caisse au 30.09.2013.
Le Conseil communal est invité à prendre connaissance du procès-verbal de vérification de la caisse au 30.09.2013 de la zone de police établi par Madame S. MARCOUX, Échevine des Finances.
31. Marchés publics – Entretien extraordinaire de la voirie et de l’infrastructure en cours d’exécution – Entretien de voiries diverses – Droit de tirage 2010-2012 – Exercice 2011 – Avenant n°1 – Approbation.
Il est proposé au Conseil communal d’approuver un avenant n°1 au marché dont question ci-dessus afin de permettre la réalisation de travaux supplémentaires pour un montant total en plus de 372.518,57 € TVA comprise et de prolonger le délai d’exécution du marché de 20 jours ouvrables.
32. Marchés publics – Voirie – Entretien extraordinaire de la voirie et de l’infrastructure en cours d’exécution – Mode doux Bouton d’Or – Avenant n°1 – Approbation.
Il est proposé au Conseil communal d’approuver un avenant n°1 au marché dont question ci-dessus afin de permettre la réalisation de travaux supplémentaires pour un montant total en plus de 40.861,70 € TVA comprise et de prolonger le délai d’exécution du marché de 20 jours ouvrables.
33. Marchés publics – Voirie – Travaux de voirie en cours d’exécution – Aménagement de la chaussée d’Ophain depuis la percée Denolin jusqu’à la rue Timpe et Tard – Avenant n°2 – Approbation.
Il est proposé au Conseil communal d’approuver un avenant n°2 au marché dont question ci-dessus afin de permettre la réalisation de travaux supplémentaires pour un montant total en plus de 148.306,07 € TVA comprise et de prolonger le délai d’exécution du marché de 20 jours ouvrables.
34. Marchés publics – Centres culturels et de loisirs – Équipement et maintenance extraordinaire en cours d’exécution des bâtiments – Centre culturel – Mise en conformité du bâtiment – Principe – Lots 1 et 2 – Projet – Devis – Mode de marché.
Il est proposé au Conseil communal de marquer son accord de principe sur la mise en conformité du réseau électrique et de la signalisation des sorties de secours du Centre culturel dont le montant estimatif de la dépense est fixé à 24.774,75 €.
35. Marchés publics – Centres culturels et de loisirs – Équipement et maintenance extraordinaire en cours d’exécution des bâtiments – Salle d’Ophain – Réfection de la toiture – Principe – Lots 1 et 2 – Projet – Devis – Mode de marché.
Il est proposé au Conseil communal de marquer son accord de principe sur la réfection de la toiture de la salle d’Ophain dont le montant estimatif de la dépense est fixé à 24.923,50 € TVA comprise.
36. Marchés publics – Enseignement primaire de Lillois – Équipement et maintenance extraordinaire en cours d’exécution des bâtiments – Fourniture et pose d’un système complet de surveillance – Projet – Devis – Mode de marché.
Il est proposé au Conseil communal de marquer son accord de principe sur l’installation d’un système de surveillance par caméras IP afin d’assurer la sécurité dans les entrées et les espaces extérieurs de l’école communale de Lillois et dont le montant estimatif de la dépense est fixé à 19.965,00 € TVA comprise.
37. Marchés publics – Parcs et plantations – Aménagement de terrain en cours d’exécution – Structure de jeux au Parc Bourdon – Projet – Devis – Mode de marché.
Il est proposé au Conseil communal de marquer son accord de principe sur l’aménagement d’une structure de jeux au Parc Bourdon dont le montant estimatif de la dépense est fixé à 69.998,50 € TVA comprise.
38. Marchés publics – Divers services communaux – Achat de matériel informatique – Principe – Lots 1à 9 – Projet – Devis – Mode de marché.
Il est proposé au Conseil communal de marquer son accord sur l’acquisition de divers équipements informatiques destinés à différents services communaux dont le montant estimatif de la dépense est fixé pour l’ensemble des lots à 76.484,10 € TVA comprise.
39. Marchés publics – Budget extraordinaire 2013 – Modification budgétaire n°2 – Mode de passation et conditions de certains marchés.
Il est proposé au Conseil communal d’arrêter la liste des marchés de faible importance et dont les dépenses sont inscrites au budget extraordinaire de l’exercice 2013.
40. Service Travaux – Rue du Jardinier – Échange de terrains avec la S.P.R.L. Property & Advice.
Il est proposé au Conseil communal d’acquérir de la S.P.R.L. Property & Advice le bien cadastré 1ère division, section N, N°548 pie d’une contenance de 38 centiares et de céder à cette même S.P.R.L. le bien cadastré 1ère division, section N, n°40 K, d’une contenance de 14 centiares.
41. Service Travaux – Collecteur du Hain – Lot 4 (parties 2 et 3) – Travaux de pose d’un pertuis préfabriqué à la rue Gaston Dubois – Convention de collaboration IBW/Commune de Braine-l’Alleud.
Il est proposé au Conseil communal d’approuver la convention de collaboration à passer entre l’I.B.W. et la Commune relative à l’exécution des travaux de pose d’un pertuis préfabriqué à la rue Gaston Dubois, conjointement à l’entreprise de pose du collecteur du Hain et dont le montant estimatif de la dépense est arrêté à 256.945,77 € TVA comprise, hors révisions et frais d’études.
42. Régie foncière et immobilière - Fixation du loyer et des conditions de location de l’appartement sis rue Jules Hans, 4 à Braine-l’Alleud.
Le Conseil communal en séance du 30.09.2013 ayant décidé de transférer le logement situé dans le bâtiment occupé par le Centre culturel dans le patrimoine privé géré par la R.F.I. en vue de sa mise en location, celui-ci est invité à en fixer les conditions de location.
43. Régie foncière et immobilière - Code wallon du logement et de l’habitat durable – Ancrage communal – Programme d’actions en matière de logement pour la période 2014-2016.
Le Conseil communal est invité à approuver le programme communal d’actions en matière de logement pour la période 2014-2016 portant sur les opérations suivantes, classées par ordre de priorité :
 création de deux appartements à la chaussée d’Ophain, 113 par le CPAS création d’un pôle social pour les aînés (15 appartements) à la rue du Paradis, 3 par le CPAS construction de 18 logements à la rue du Vignoble par la SCRL Habitations sociales du Roman Païs construction de 3 logements de transit à la rue Longue, 34 par le CPAS construction de 16 maisons, 20 appartements et équipement de 10 parcelles bâtissables au Champ Saint-Zèle par la SCRL Habitations sociales du Roman Païs opération non localisée : prise en gestion de 3 logements par l’Agence immobilière sociale.
44. Questions diverses (Article 79 du Règlement d’Ordre intérieur).
45. Procès-verbal de la séance publique du 30.09.2013.
