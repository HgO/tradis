#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import signal
import argparse


from PyQt4 import QtGui
from mainwindow import MainWindow

def sigint_handler(*args):
    """Handler for the SIGINT signal."""
    sys.exit(0)

def argParser():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-o', '--output', dest="output_file", metavar="output_file", nargs='?',
        help='Output file name')
        
    action = parser.add_mutually_exclusive_group(required=False)
    action.add_argument('-i', '--import', dest="import_file", metavar="import_file", nargs='?',
        help='Import an agenda from file')
    action.add_argument('-u', '--url', dest="import_url", metavar="import_url", nargs='?',
        help='Import an agenda from url')
    action.add_argument('open_file', nargs='?',
        help='Input file name')           
                   
    return parser.parse_args()

if __name__=='__main__':
    signal.signal(signal.SIGINT, sigint_handler)
    
    args = argParser()
    
    app = QtGui.QApplication(sys.argv)
    window = MainWindow(args)
    app.exec_()
