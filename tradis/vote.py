#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Vote(object):
    """Represents a vote for agenda's element"""
    _pro = 0
    _blank = 0
    _con = 0
    
    def pro(self):
        return self._pro
        
    def setPro(self, pro):
        self._pro = pro
        
    def blank(self):
        return self._blank
        
    def setBlank(self, blank):
        self._blank = blank    
    
    def con(self):
        return self._con
        
    def setCon(self, con):
        self._con = con
    
    def toDict(self):
        """Convert the vote class into a dictionnary"""
        dict = {}
        for key, value in self.__dict__.items():
            if value:
                dict[key[1:]] = value
        
        return dict
        
    def __repr__(self):
        return "+" + str(self._pro) + "/" + str(self._blank) + "/-" + str(self._con)

    def update(self, entries):
        """Read a dictionnary to populate the class"""

        for key, value in entries.items():
            attr = "_" + key
            if hasattr(self, attr):
                if isinstance(value, int) and value >= 0:
                    self.__dict__[attr] = value
