#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt4 import QtGui, QtCore
from datetime import datetime

from ui.mainwindow import Ui_MainWindow
from mainmodel import MainModel

class MainWindow(QtGui.QMainWindow):    
    def __init__(self, args=None, parent=None):
        super().__init__(parent)
        
        self._model = MainModel(self, args)
        
        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)

        self.centerOnScreen()
        self.show()

        QtGui.QShortcut(QtGui.QKeySequence("Ctrl+Q"), self, QtGui.QApplication.quit)
        QtGui.QShortcut(QtGui.QKeySequence("Alt+Left"), self, self.prevElement)
        QtGui.QShortcut(QtGui.QKeySequence("Ctrl+P"), self, self.prevElement)
        QtGui.QShortcut(QtGui.QKeySequence("Alt+Right"), self, self.nextElement)
        QtGui.QShortcut(QtGui.QKeySequence("Ctrl+N"), self, self.nextElement)
        QtGui.QShortcut(QtGui.QKeySequence("Ctrl+L"), self, self.focusNavSpinBox)
    
        self._ui.tableWidget.installEventFilter(self)
        self._ui.prevPushButton.installEventFilter(self)
        self._ui.nextPushButton.installEventFilter(self)

        #self._ui.notesTextEdit.setMarginLineNumbers(1, True)
        #self._ui.notesTextEdit.setMarginWidth(1, "1000")
        self._ui.notesTextEdit.enableFindReplace(True)

        self._model.processArgs()

    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.KeyPress:
            if event.key() == QtCore.Qt.Key_Return:
                if source is self._ui.tableWidget:
                    index = source.currentRow()
                    if index >= 0:
                        self.selectElement(index)
                elif source is self._ui.prevPushButton:
                    self.prevElement()
                elif source is self._ui.nextPushButton:
                    self.nextElement()

        return QtGui.QWidget.eventFilter(self, source, event)
    
    def resizeEvent(self, event):
        self.stretchTableWidget()
            
        super().resizeEvent(event)
    
    def stretchTableWidget(self):
        self._ui.tableWidget.horizontalHeader().resizeSections(QtGui.QHeaderView.Stretch)

    def update(self, id, elements):
        if len(elements):
            self._ui.elementTab.setEnabled(True)
            self._ui.progressBar.setMaximum(len(elements))
            self._ui.navSpinBox.setMaximum(len(elements))

            titles = [element.title() for element in elements]
            self._ui.navComboBox.clear()
            self._ui.navComboBox.addItems(titles)
            self._ui.navComboBox.setCurrentIndex(id)
        else:
            self._ui.elementTab.setEnabled(False)
        
    def updateOverview(self, id, elements, location, date):
        if len(elements):
            self._ui.tableWidget.setRowCount(len(elements))

            for i in range(len(elements)):
                self._ui.tableWidget.setItem(i, 0, QtGui.QTableWidgetItem(elements[i].title()))
                self._ui.tableWidget.setItem(i, 1, QtGui.QTableWidgetItem(elements[i].description()))
                self._ui.tableWidget.setItem(i, 2, QtGui.QTableWidgetItem(elements[i].notes()))

            index = self._ui.tableWidget.model().index(id, 0)
            self._ui.tableWidget.setCurrentIndex(index)

        self.stretchTableWidget()

        self._ui.locationLabel.setText(str(location))

        date = date.strftime("Le %d/%m/%y à %Hh%M") if date else ""
        self._ui.dateLabel.setText(date)

    def updateElement(self, id, element):
        vote = element.vote()
        
        self._ui.progressBar.setValue(id+1)
        
        self._ui.navSpinBox.setValue(id+1)
        self._ui.navComboBox.setCurrentIndex(id)
        
        self._ui.titleValueLabel.setText(element.title())
        self._ui.descriptionValueLabel.setText(element.description())
        
        self._ui.votesValueLabel.setText(str(vote.pro() - vote.con()))
        self._ui.proSpinBox.setValue(vote.pro())
        self._ui.blankSpinBox.setValue(vote.blank())
        self._ui.conSpinBox.setValue(vote.con())
        
        self._ui.notesTextEdit.setPlainText(element.notes())

    def centerOnScreen(self):
        resolution = QtGui.QDesktopWidget().screenGeometry()
        self.move((resolution.width() - self.frameSize().width()) / 2,
                  (resolution.height() - self.frameSize().height()) / 2)
                  
    def nextElement(self):
        self._model.nextElement()
        
    def prevElement(self):
        self._model.prevElement()
    
    def elementAtValue(self, value):
        self.elementAt(value-1)
    
    def elementAt(self, index):
        if index >= 0:
            self._model.elementAt(index)
        
    def proVote(self, value):
        self._model.proVote(value)
    
    def blankVote(self, value):
        self._model.blankVote(value)
    
    def conVote(self, value):
        self._model.conVote(value)
    
    def saveNotes(self):
        text = self._ui.notesTextEdit.toPlainText()
        self._model.saveNotes(text)
        
    def changeTab(self):
        if self._ui.tabWidget.currentIndex() == 0:
            self._model.notifyOverview()
        
    def selectElement(self, index):
        self.elementAt(index)

        self._ui.tabWidget.setCurrentWidget(self._ui.elementTab)

    def importFile(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Importer...")
        if filename:
            self._model.importFile(filename)
        
    def openFile(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Ouvrir...")
        if filename:
            self._model.openFile(filename)
        
    def openUrl(self):
        url, ok = QtGui.QInputDialog.getText(self, "Importer depuis une url...", "Url:")
        
        if ok:
            self._model.importUrl(url)
        
    def saveFile(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Enregistrer...")
        if filename:
            self._model.saveFile(filename)

    def saveDraft(self):
        self._model.saveDraft()

    def focusNavSpinBox(self):
        self._ui.navSpinBox.setFocus()
        self._ui.navSpinBox.selectAll()

    def shortcutsDialog(self):
        QtGui.QMessageBox.about(self, "Raccourcis clavier",
"""Ctrl+A : À propos
Ctrl+I : Importer
Ctrl+L : Aller à
Ctrl+N / Alt+Droite : Élément suivant
Ctrl+O : Ouvrir
Ctrl+P / Alt+Gauche : Élément précédent
Ctrl+Q : Quitter
Ctrl+S : Sauvegarder""")

    def aboutDialog(self):
        pass
