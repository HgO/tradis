#!/usr/bin/python3
# -*- coding: utf-8 -*-

from vote import Vote

class Element(object):    
    """Class representing an element from the council's agenda

    An element has a title, a description and notes. It also counts votes."""

    def __init__(self, title = "", description = "", notes = ""):
        self._title = title
        self._description = description
        self._notes = notes
        self._vote = Vote()
    
    def vote(self):
        return self._vote

    def title(self):
        return self._title
        
    def description(self):
        return self._description
        
    def notes(self):
        return self._notes

    def setTitle(self, title):
        self._title = title

    def setDescription(self, description):
        self._description = description

    def setNotes(self, text):
        self._notes = text
    
    def toDict(self):
        """Convert the element class to a dictionnary."""
        dict = {}
        for key, value in self.__dict__.items():
            if isinstance(value, Vote):
                value = value.toDict()
            
            if value:
                dict[key[1:]] = value
                
        return dict
    
    def __repr__(self):
        return "Title: " + str(self._title) + "\n" + \
            "Description: " + str(self._description) + "\n" + \
            "Vote: " + str(self._vote) + "\n" + \
            "Notes: " + str(self._notes)

    def update(self, entries):
        """Read a dictionnary to populate the class"""
        for key, value in entries.items():
            attr = "_" + key
            if hasattr(self, attr):
                if key == "vote":
                    self._vote.update(value)
                    self.__dict__[attr] = self._vote
                elif isinstance(value, str):
                    self.__dict__[attr] = value
