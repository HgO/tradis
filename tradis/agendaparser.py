#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import urllib.request
from datetime import datetime
import dateutil.parser

from element import Element

class AgendaParser:
    """Parse a file containing the agenda.

    This file must be a json file and can be found at www.droitderegard.be"""

    def __init__(self, filename):
        self.elements = AgendaParser.parse(filename)

    def parse(filename):
        elements = []
        location = ""
        date = None

        stream = open(filename, "r")
        try:
            items = json.load(stream)
        
            oj = AgendaParser.searchParent(items[0]["Content"], "Séance publique")
            
            hasDescription = "h class=\"obj\"" in oj[1]
            oj = list(oj[1].values())[0]
            
            elements = AgendaParser.makeElements(oj, hasDescription)
            
            infos = items[1]["Enveloppe"][0]["id_ddr"]
            date = dateutil.parser.parse(infos.split("_")[0], fuzzy = True)
            
            location = items[0]["Content"][1]["h"][1]["p"][0]
        finally:
            stream.close()
            
        return elements, location, date
        
    def parseUrl(url):
        filename, headers = urllib.request.urlretrieve(url)
        
        return AgendaParser.parse(filename)
        
    def makeElements(oj, hasDescription):
        """Converts a list into the agenda."""
        elements = []
        element = None
        isTitle = True
        for item in oj:
            if isTitle:
                if isinstance(item, dict):
                    elements.extend(AgendaParser.makeElements(list(item.values())[0], hasDescription))
                else:
                    element = Element()
                    element.setTitle(item)
                    elements.append(element)
            else:
                if isinstance(item, dict):
                    element.setDescription(AgendaParser.toPlainText(item["p"]))
                
            if hasDescription:
                isTitle = not isTitle 
        return elements        
        
    def searchParent(items, value):
        """Search into the json file the list or dict containing a certain value

    The json file contains lists and dicts. We need to check recursively if the
    current item is a list or a dict. If it's a dict, we convert it into a list.
    If the list contains the value searched, then we return this list."""

        result = None

        if isinstance(items, dict):
            items = list(items.values())

        if isinstance(items, list):
            for item in items:
                if value == item:
                    result = items
                else:
                    result = AgendaParser.searchParent(item, value)

                if result:
                    break

        return result

    def toPlainText(content):
        """Convert the json file's structure into plain text"""
        if isinstance(content, dict):
            content = list(content.values())

        if isinstance(content, list):
            content = [AgendaParser.toPlainText(item) for item in content]

            return "\n".join(content)

        return content

    def results(self):
        return self.elements
