# Introduction

Tradis is a notetaking application for belgian city council's agendas.

Using the agendas (which are json files) located on www.droitderegard.be, Tradis allows to easily take 
notes and count votes during the city council's meeting.

# Requirements

- Python3
- PyQt4 (python3-qt4)
- PyKDE4 (python3-kde4)


